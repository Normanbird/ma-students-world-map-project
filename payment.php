
<h2>So you need to send us a payment via a credit card? Simply use the form below.</h2>

<form action="" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_0KA0tWn1JDgaBQhXKdhJXaVi06Msp"
    data-amount="2000"
    data-name="Demo Site"
    data-description="2 widgets ($20.00)"
    data-image="/128x128.png">
  </script>
</form>