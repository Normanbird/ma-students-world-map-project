<?php 

require_once('includes/c1.php');
require_once('includes/functions.php');
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head> 
   <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Mastering Alchemy Students Terrestrial Earth Regions System</title>
    <meta name="description" content="Update location in profile of Alchemy Student's. Spirituality" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">   

<body>
<div class="container col-md-12">  
<div class="changepassword"><h2>Email New Password</h2><p >Back to <a href="login.php">login</a></p></div><br>
	<div class="row ">
		<div class="col-lg-12 bmargin">            	
           <div class="updateArea updateMessage"></div>
				<div name="forgotform" class="forgotForm" >
					<h4>Enter the EMAIL address that is associated with your account and we will reset the associated password and email it to you:</h4>	
					<input name="email" type="text" id="email1" size="40" maxlength="50" class="form-control" />
					<button name="button" class="btn btn1 btn-primary"  >Reset &amp; email me a new password</button>
				</div> 

		</div> <!-- col-lg-12  centerme-->	
	</div> <!-- row -->	
</div> <!--container -->
<br />

<?php require_once('footer.php'); ?>