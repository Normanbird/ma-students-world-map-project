<?php
require_once('includes/init.php');


//get id from session 
$id = $_SESSION['memberid'];
// PREPARE sql STATEMENT. 35 variables
 // $sql = "Select * from members WHERE memberid = $id AND active = 1 limit 1";
 $selectsql = "Select * from members where memberid = $id limit 1";

  $results = $c1->query($selectsql) or die(mysqli_error($c1));
    $row = mysqli_fetch_assoc($results);

//check if member owns record or is admin
// $authorized = false;
//   if ($_SESSION['did_admin'] or ($_SESSION['memberid'] == $id) ) { 
//     $authorized = true;
//     }
 ?>

 <!-- lets display form in edit mode for admin user -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="style.css">
    <script type="text/javascript">


function submitform1() 
{    
  if (window.document.forms[0].password.value == "")
            {
            alert ("\n Please enter a password");
            document.forms[0].password.focus();
            document.forms[0].password.style.backgroundColor="yellow";
            return false;
            }
              
  if (document.forms[0].email.value == "")
            {
            alert ("\n Please enter your email address.");
            document.forms[0].email.focus();
            document.forms[0].email.style.backgroundColor="yellow";
             }             
    if (document.forms[0].lastname.value == "")
            {
            alert ("\n Please enter your Last Name.");
            document.forms[0].lastname.focus();
            document.forms[0].lastname.style.backgroundColor="yellow";
            return false;
            }
    if (document.forms[0].firstname.value == "")
            {
            alert ("\n Please enter your First Name.");
            document.forms[0].firstname.focus();
            document.forms[0].firstname.style.backgroundColor="yellow";
            return false;
            }
            
  return true;           
}
</script>	
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	   
    <?php include('menu.php'); ?>

       <!-- start of container page -->

<div class="container col-md-12">  
  <div class="row ">
    <div class="col-lg-12 ">

      <h2 class="profile">Edit Your Profile</h2>
        <ul class="submenu">
               <li><a href="change-password.php">change password</a></li>
               <li><a href="update-location.php">update location</a></li>
        </ul>   

      <form  action="process-edit-profile.php" method="post" id="profileForm"  name="profileForm" role="form" onSubmit="return submitform1();" >
       
    <?php if (!empty($_GET) ) {                 
        
      $strError = '<div class="formerror"><p><img src="images/error.png" width="32" height="32" hspace="5" alt="">Please check the following and try again:</p><ul>';
                    
          if (!empty($_SESSION['myarray']) )
          {           
                  foreach ($_SESSION['myarray'] as $error) {
                      $strError .= "<li>$error</li>";
                  }
          }
                  $strError .= '</ul></div>';
                
                 echo $strError;
            
           
        } //if GET
     
    ?> 
          <table id="table1" width="100%" border="0" cellspacing="1" cellpadding="2">
           <tr>
              <td width="30%"><label>Email*</label></td>
              <td width="20%"><input  class="form-control" name="email" type="text" id="email" size="40" maxlength="50" 
              value="<?php if (isset($row['memberemail'])) {echo $row['memberemail']; }?>"/></td>
               <td width="35%"><span>( Enter your Email address )</span></td>
            </tr>
                    
           <!--  <tr>
              <td><label>Password*</label></td>
              <td><input  class="form-control" name="password" type="password" id="password" size="20" maxlength="20" 
              value=""/> 
              </td>
              <td><span>( up to 15 characters )</span></td>
            </tr> -->
             
            <tr>
              <td><label>Last Name*</label></td>
              <td><input  class="form-control" name="lastname" type="text" id="lastname" size="40" maxlength="50" 
              value="<?php if (isset($row['lastname'])) {echo $row['lastname']; }?>"/></td>
            </tr>
            <tr>
              <td><label>First Name*</label></td>
              <td><input  class="form-control" name="firstname" type="text" id="firstname" size="40" maxlength="50" 
              value="<?php if (isset($row['firstname'])) {echo $row['firstname']; }?>"/></td>
            </tr>  
          
              
             <tr>
              <td><label>Student Level*</label></td>
              <td>
              <select name="class" type="text" required >
              <option <?php if ($row['class'] == 'l1')   echo "selected=\"selected\""; ?> value="l1"  >Level 1</option>
              <option <?php if ($row['class'] == 'l2')   echo "selected=\"selected\""; ?> value="l2"  >Level 2</option>
              <option <?php if ($row['class'] == 'l3')   echo "selected=\"selected\""; ?> value="l3"  >Level 3</option>
              <option <?php if ($row['class'] == 'l3l')  echo "selected=\"selected\""; ?> value="l3l" >Level 3 Live</option>
              </select>
              </td>
            </tr>  

            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          
            <tr>
              <td>&nbsp;</td>
              <td><button type="submit" name="finish" id="finish" class="btn btn-primary" >Update Profile</button></td>
            </tr>
          </table>
          <input type="hidden" name="id" value="<?php echo $id; ?>">
        </form>

         
    </div> <!-- col-lg-12  centerme-->  
  </div> <!-- row --> 
</div> <!--container -->
<br />

        <?php require('footer.php') ?>     