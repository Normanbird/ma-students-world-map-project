<?php //require_once('includes/init.php'); ?>
<?php 
session_start();

require_once('includes/c1.php'); 
require_once('includes/functions.php'); 

date_default_timezone_set('US/Eastern');
?>
<?php
// *** Validate request to login to this site.

if (isset($_POST['userfield']) && ($_POST['userfield'] != "") ) {
  $loginUsername=$_POST['userfield'];
  $password=$_POST['passwordfield'];
  $password = urlencode($password);

  //query DB and gethashed password
    $Loginquery=sprintf("SELECT * FROM members WHERE memberemail='%s' ",
    get_magic_quotes_gpc() ? $loginUsername : addslashes($loginUsername) );      

  $LoginRS = $c1->query($Loginquery) or die(mysql_error());

  if ( $results = $LoginRS->fetch_array() ) {  
  $user = $results["memberemail"];
  $hashed = $results["memberpassword"];
  $memberid = $results["memberid"];

   //then simply check it against the hashed password
  $loginFoundUser = '';

  if (bcrypt_check_hash($password, $hashed) ) {  $loginFoundUser = true;  }
} else
    $loginFoundUser = false;
 
  //psssword is correct so lets continue
  if ($loginFoundUser) {      
    $_SESSION["UserEmail"] = $loginUsername;
    $_SESSION["memberid"] = $memberid;

       //log user to DB log
    $logusers = new processuser();
                $logusers->loguser();
                
    if (isset($_SESSION['PrevUrl']) && true) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];  
      echo "session prevurl not set";
    }
    
    header("Location: ./index.php");

  }
  else {
    $failedlogin = 'failedlogin';
    header("Location: ./login.php?failedlogin=". $failedlogin);    
  }

} //if POST varible for userfield is set
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="style.css">
  
  
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login">
    

      <div class="container loginform">          
        <br>

          <form id="form1" name="form1" method="post" class="form-signin" action="<?php $_SERVER['PHP_SELF'] ?>">
                      

            
            <img class="loginimg img-responsive" src="images/login.jpg" alt="World Map Project Dragon fly"><br>
            <!-- <h1 class="text-center">Welcome!</h1> -->
<!--            <p>Welcome to the Mount Vernon Yacht Club members area. Please enter your username and password Below.</p>
 -->            <p>If this is your first time, please <a href="register.php">Register</a>.</p>
 <p>If you are already a member enter your eMail and password below.</p>
 <p>If you don't remember your password, simply click <a href="forgotpassword.php">can't access account</a> to have it emailed to you.</p>
            <h3 class="form-signin-heading text-center">Please sign in</h3>
            <h3>(Active MA Students Only Please!)</h3>
            <?php if (isset($_GET['failedlogin']) == 'register.php') {
              echo '<div class="alert alert-danger"> Incorrect email and password combination </div>';
              
              }?>

            <label for="userfield"  class="login-label">Email Address</label>
            <input name="userfield"  maxlength="50" type="text" class="form-control" placeholder="Email address" autocomplete="on" autofocus>
            <label for="passwordfield" class="login-label">Password</label>
            <input name="passwordfield" type="password"  maxlength="50" type="password" class="form-control" placeholder="Password">
            <!-- <label class="checkbox">
              <input type="checkbox" value="remember-me"> Remember me
            </label> -->
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button><br>
            <p class="cantaccess text-center"> <a href="forgotpassword.php">Can't access account?</a> &nbsp; - &nbsp;<a href="register.php">Register</a></p>
        </form>   
<br><br><br><br><br><br>
        <?php require('footer.php') ?>  