<?php
require_once 'includes/init.php';  

if (isset($_POST['latitude'])) {
 

 $latitude = $_POST['latitude'];
 $longitude = $_POST['longitude'];
 $id = $_SESSION['memberid'];

  $updatesql = "UPDATE members SET latitude = ?, longitude=? WHERE memberid = ? ";
  /* Prepare statement */
  $stmt = $c1->prepare($updatesql);
  if($stmt === false) {
    trigger_error('Wrong SQL: ' . $updatesql . ' Error: ' . $c1->error, E_USER_ERROR);
  }
  /* Bind the 42 parameters. TYpes: s = string, i = integer, d = double,  b = blob */
  $stmt->bind_param('ddi', $latitude, $longitude, $id);       
  /* Execute statement */
  $stmt->execute();

  header('location: ./index.php'); 

}
?>


<!-- display get coordinates page -->
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head> 
          <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Mastering Alchemy Students Terrestrial Earth Regions System</title>
    <meta name="description" content="Update location in profile of Alchemy Student's. Spirituality" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">   
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
  
  var map;
  var geocoder;
  var marker;
  
  function initialize() {
    var latlng = new google.maps.LatLng(48.379433, 31.165580);
    var myOptions = {
      zoom: 5,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP 
    };
    map = new google.maps.Map(document.getElementById("latlongmap"),
        myOptions);
  geocoder = new google.maps.Geocoder();
  marker = new google.maps.Marker({
      position: latlng, 
      map: map
  });
  
map.streetViewControl=false;

  google.maps.event.addListener(map, 'click', function(event) {
    marker.setPosition(event.latLng);
    
    var newpos = event.latLng;
    document.getElementById('lat').value=newpos.lat().toFixed(6);
    document.getElementById('lng').value=newpos.lng().toFixed(6);
    // document.getElementById('coordinatesurl').value = 'http://www.latlong.net/c/?lat=' 
    //                     + newpos.lat().toFixed(6) + '&long='
    //                     + newpos.lng().toFixed(6);
  });
    

// google.maps.event.addListener(map, 'mousemove', function(event) {
// var newpos = event.latLng;
// document.getElementById("mlat").value = newpos.lat().toFixed(6);
// document.getElementById("mlong").value = newpos.lng().toFixed(6);
// });

  }

function codeAddress() {
    var address = document.getElementById("gadres").value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
    document.getElementById('lat').value=results[0].geometry.location.lat().toFixed(6);
    document.getElementById('lng').value=results[0].geometry.location.lng().toFixed(6);
var latlong = "(" + results[0].geometry.location.lat().toFixed(6) + " , " +
  + results[0].geometry.location.lng().toFixed(6) + ")";
// document.getElementById('coordinatesurl').value = 'http://www.latlong.net/c/?lat=' 
//                         + results[0].geometry.location.lat().toFixed(6) + '&long='
//                         +results[0].geometry.location.lng().toFixed(6);


 var infowindow = new google.maps.InfoWindow({
        content: latlong
    });

        marker.setPosition(results[0].geometry.location);
        map.setZoom(12);

google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

      } else {
        alert("Lat and long cannot be found.");
      }
    });
  }


function process1() {
// ad lat and lang value to our submitted forms hidden fields to pass to POST
  document.getElementById('lat2').value = document.getElementById('lat').value;
  document.getElementById('lng2').value = document.getElementById('lng').value;
    
}

</script>
<style>
    #gadres { width: 320px; max-width: 320px;}
</style>
</head> 
<body onload="initialize()"> 
  <?php include('menu.php'); ?>
   
<div class="container update">
    
    <!-- address box to find lat and lang -->
    <div class="eleven columns">
         <div class="box">

            <label for="gadres">Address</label>
            <input id="gadres" type="text" size="24" onclick="this.select();" placeholder="Type address here to find its location on map" /> 
            <button title="Find Lat & Long" onclick="codeAddress();" />Find Lat Long</button>

        </div>
        
                 <p>You can hold and move marker to anywhere you want to get the lat long of near places of your address. This geo process is also known as <em>geocode address</em>.</p>
        <div id="latlongmap" style="width:100%; height:420px;">
        </div>
        <!-- <label for="coordinatesurl">Share this location: 
            <input type="text" id="coordinatesurl" readonly="readonly" style="width:75%;max-width:460px;" onclick="this.select();" />
       </label>
         -->

        <div class="box latlong">

            <label for="lat">Latitude</label>
            <input type="text" name="lat" id="lat" readonly="true" />

            <label for="lng">Longitude</label>
            <input type="text" name="lng" id="lng" readonly="true" />

        </div>
       <p>AFTER you have found your latitude and longitude coordinates and they appear in the above boxes. Click SUBMIT below to update these coordinates  to your profile.</p>


       <form name="form1" action="<?php $_SERVER['PHP_SELF']; ?>" method="post" onSubmit="process1();">

        <input type="hidden" name="latitude" id="lat2" value="" />
        <input type="hidden" name="longitude" id="lng2" value="" />
        <button type="submit" class="lookup-btn" >Submit the above coordinates to my profile!</button>
       </form>
    </div> <!-- eleven columns -->
</div>   <!-- container -->

<script type="text/javascript">
    $(document).ready(function() {
        $("#gadres").keyup(function(event){
            if(event.keyCode == 13){
                codeAddress();
            }           
         
        });

       
    });

</script>

 <?php require_once('footer.php') ?>  