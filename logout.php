<?php 
session_start();

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION = array(); session_destroy();
  
  $logoutGoTo = "login.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}//end if isset doLogout 


$did_restrictGoto = "login.php";
if (!isset($_SESSION['UserEmail'])) {  

  $did_qsChar = "?";
  $did_referrer = $_SERVER['PHP_SELF'];
  if (strpos($did_restrictGoto, "?")) $did_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $did_referrer .= "?" . $QUERY_STRING;
  $did_restrictGoto = $did_restrictGoto. $did_qsChar . "accesscheck=" . urlencode($did_referrer);
  header("Location: ". $did_restrictGoto); 
  exit;
}
?>