<?php
require_once('includes/c1.php');
require_once('includes/functions.php');
	
// start session
//code only runs if the GET variable ajax is true
if ($_GET['metatron'] == true) {	
	
	$email = mysqli_real_escape_string($c1,$_POST['email_field']);
		
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	   echo '<div class="alert-danger custom-message"><h4>You must enter a valid email address.</h4></div>';
		exit;
		// email is valid
	}

//check database for match of email. if found continue and reset	
$sql = "SELECT * FROM `members` WHERE memberemail = '$email' ";
      /* Prepare statement */
      $results = $c1->query($sql);
      if($results == false) {
        trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $c1->error, E_USER_ERROR);
      }
     
	
		$count = $results->num_rows;
	
		if ($count > 0) {
					$r = $results->fetch_assoc();
					$email = $r['memberemail'];				
		} else {
			echo '<div class=" alert alert-danger custom-message"><h4>sorry, We do not have a record of anyone registered with that email address.</h4>';
			echo '<h4>Please recheck or use the email address you registered with, thanks.</h4>';
			echo '<h4>Try again, or go to <a href="login.php">Login Page</a></h4></div>';
			exit;
		}
					
		//email found in system. lets reset & generate new password to email to user.
		$new_password = generate_new_password($email);

//ajax will show this success message
$message = '<div class="alert-success custom-message"><h2> A reminder email was sent to '. $email. ' and you should receive it in a few moments. Thanks!</h2>';
$message .='****************************************************************************************';
$message .= '<h4>CHECK YOUR SPAM FOLDER IF YOU DONT SEE IT SOON</h4>';
$message .= '<h4>Go to the <a href="login.php">Login Page</a></h4></div>';	

echo $message;


} //if GET ajax=true 
 ?>