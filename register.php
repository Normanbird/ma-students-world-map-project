<?php session_start();

require_once('includes/c1.php');
require_once('includes/functions.php');
//require_once('includes/functions.php'); ?>
<?php
// ** Logout the current user. **

$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  $_SESSION = array(); session_destroy();
	
  $logoutGoTo = "login.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!empty($_POST)) {
	$salt = 'alchemy';
	$email = $_POST['email'];
	$email2 = $_POST['email2'];
	$password = $_POST['password'];
	$password = urlencode($password);
	$password2 = $_POST['password2'];
	
	$numfield = $_POST['numfield'];
	$lastname = $_POST['lastname'];
	$firstname = $_POST['firstname'];

  //new fields
  // $country = $_POST['country'];
  // $city = $_POST['city'];
  // $zip  = $_POST['zip'];
  // $latitude = $_POST['latitude'];
  // $longitude = $_POST['longitude'];
  $class = $_POST['class'];

	
	//read post variables from form into variables using class
	$m = array();
	$user = new processuser;
	//check if username already in use in DB
	if ($user->checkuserexist($_POST)) {		
		$m[] = 'That Email addess is already being used in our system and can not be resued. If it is your Email address, try <a href="forgotpassword.php">resetting the password.</a>' ;
	}
			
	if ($password == "" || strlen($password) > 15 || strlen($password) < 4) {					
			$m[] = "Password length must be between 4 & 15 characters"; 
	}
																
	if ($password2 != $password) {			
			$m[] = "The two passwords entered do not match. Please recheck."; 
	}		
		
	if (!preg_match('/^[_A-z0-9-]+((\.|\+)[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,})$/',$email)) {			
			$m[] = "Please check that you have entered a valid email address"; 
	}	
										
	if ($email2 != $email) {			
			$m[] = "The two email addresses entered do not match. Please recheck."; 									
	}					
										
	if ($_POST['num1'] + $_POST['num2'] != $_POST['numfield']) {			
			$m[] = "You failed the human checker, please enter the sum of the numbers correctly"; 
	}		
															
	if (!isset($_POST['agree'])) {					
			$m[] = "Terms of service agreement must be checked"; 
	}													
	
	if ($lastname == "" || strlen($lastname) > 25 || strlen($lastname) < 2) {					
			$m[] = "Last Name must be between 2 & 25 characters"; 
	}

	if ($firstname == "" || strlen($firstname) > 25 || strlen($firstname) < 2) {					
			$m[] = "First Name must be between 2 & 25 characters"; 
	}


	if ( count($m) > 0 ) { 		
		// there is an error in fields filled out so we are sending user back to form.
		$_SESSION["myarray"] = $m;
		$name = "";
		header("location: ./register.php?email=".$email."&email2=".$email2."&lastname=".$lastname."&firstname=".$firstname);
		echo "header isnt firing line 82 of register.php"; exit;
	}
	//if user didnt exist, we add to db and go to second form
		
	$user->addNewRegisteredUser($email, $password, $lastname, $firstname, $class) or die("error adding new user");									
} 

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>MA Students World Map Project - Registration Form</title>
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
 <!-- <script src="js/modernizr.js"></script>-->
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="style.css">

<script src="http://api.geonames.org/export/geonamesData.js?username=databaseindays"></script>
<script src="http://www.geonames.org/export/jsr_class.js"></script>

<script type="text/javascript">
  
  // postalcodes is filled by the JSON callback and used by the mouse event handlers of the suggest box
var postalcodes;

// this function will be called by our JSON callback
// the parameter jData will contain an array with postalcode objects
function getLocation(jData) {
  if (jData == null) {
    // There was a problem parsing search results
    return;
  }

  // save place array in 'postalcodes' to make it accessible from mouse event handlers
  postalcodes = jData.postalcodes;
      
  if (postalcodes.length > 1) {
    // we got several places for the postalcode
    // make suggest box visible
    document.getElementById('suggestBoxElement').style.visibility = 'visible';
    var suggestBoxHTML  = '';
    // iterate over places and build suggest box content
    for (i=0;i< jData.postalcodes.length;i++) {
      // for every postalcode record we create a html div 
      // each div gets an id using the array index for later retrieval 
      // define mouse event handlers to highlight places on mouseover
      // and to select a place on click
      // all events receive the postalcode array index as input parameter
      suggestBoxHTML += "<div class='suggestions' id=pcId" + i + " onmousedown='suggestBoxMouseDown(" + i +")' onmouseover='suggestBoxMouseOver(" +  i +")' onmouseout='suggestBoxMouseOut(" + i +")'> " + postalcodes[i].countryCode + ' ' + postalcodes[i].postalcode + ' &nbsp;&nbsp; ' + postalcodes[i].placeName  +'</div>' ;
    }
    // display suggest box
    document.getElementById('suggestBoxElement').innerHTML = suggestBoxHTML;
  } else {
    if (postalcodes.length == 1) {
      // exactly one place for postalcode
      // directly fill the form, no suggest box required 
      var placeInput = document.getElementById("placeInput");
      var latitude = document.getElementById("latitude");
      var longitude = document.getElementById("longitude");
      placeInput.value = postalcodes[0].placeName;
      latitude.value = postalcodes[0].lat;
      longitude.value = postalcodes[0].lng;
    }
    closeSuggestBox();
  }
}


function closeSuggestBox() {
  document.getElementById('suggestBoxElement').innerHTML = '';
  document.getElementById('suggestBoxElement').style.visibility = 'hidden';
}


// remove highlight on mouse out event
function suggestBoxMouseOut(obj) {
  document.getElementById('pcId'+ obj).className = 'suggestions';
}

// the user has selected a place name from the suggest box
function suggestBoxMouseDown(obj) {
  closeSuggestBox();
  var placeInput = document.getElementById("placeInput");
  var latitude = document.getElementById("latitude");
  var longitude = document.getElementById("longitude");
  placeInput.value = postalcodes[obj].placeName;
  latitude.value = postalcodes[obj].lat;
  longitude.value = postalcodes[obj].lng;
}

// function to highlight places on mouse over event
function suggestBoxMouseOver(obj) {
  document.getElementById('pcId'+ obj).className = 'suggestionMouseOver';
}


// this function is called when the user leaves the postal code input field
// it call the geonames.org JSON webservice to fetch an array of places 
// for the given postal code 
function postalCodeLookup() {

  var country = document.getElementById("countrySelect").value;

  if (geonamesPostalCodeCountries.toString().search(country) == -1) {
     return; // selected country not supported by geonames
  }
  // display loading in suggest box
  document.getElementById('suggestBoxElement').style.visibility = 'visible';
  document.getElementById('suggestBoxElement').innerHTML = '<small><i>loading ...</i></small>';

  var postalcode = document.getElementById("zip").value;

  request = 'http://api.geonames.org/postalCodeLookupJSON?postalcode=' + postalcode  + '&country=' + country  + '&callback=getLocation' + '&style=long&username=databaseindays';

  // Create a new script object
  aObj = new JSONscriptRequest(request);
  // Build the script tag
  aObj.buildScriptTag();
  // Execute (add) the script tag
  aObj.addScriptTag();
}


// set the country of the user's ip (included in geonamesData.js) as selected country 
// in the country select box of the address form
function setDefaultCountry() {
  var countrySelect = document.getElementById("countrySelect");
  for (i=0;i< countrySelect.length;i++) {
    // the javascript geonamesData.js contains the countrycode
    // of the userIp in the variable 'geonamesUserIpCountryCode'
    if (countrySelect[i].value == geonamesUserIpCountryCode) {
      // set the country selectionfield
      countrySelect.selectedIndex = i;
    }
  }
}

function submitform1() 
{ 
   
  if (window.document.forms[0].password.value == "")
            {
            alert ("\n Please enter a password");
            document.forms[0].password.focus();
            document.forms[0].password.style.backgroundColor="yellow";
            return false;
            }
   
  if (window.document.forms[0].password2.value == "")
            {
            alert ("\n Please enter Password again");
            document.forms[0].password2.focus();
            return false;
            }
            
  if (document.forms[0].password.value != document.forms[0].password2.value)
            {
            alert ("\n The Passwords you entered do not Match \n\n Please recheck.");
            document.forms[0].password2.focus();
            return false;
            }
            
  if (document.forms[0].email.value == "")
            {
            alert ("\n Please enter your email address.");
            document.forms[0].email.focus();
            document.forms[0].email.style.backgroundColor="yellow";
            return false;
            }
            
            
  if (document.forms[0].email2.value == "")
            {
            alert ("\n Please enter your email address again.");
            document.forms[0].email2.focus();
            document.forms[0].email2.style.backgroundColor="yellow";
            return false;
            } 
            
  if (document.forms[0].email.value != document.forms[0].email2.value)
            {
            alert ("\n The Emails you entered do not Match \n\n Please enter your email address.");
            document.forms[0].email2.focus();
            document.forms[0].email2.style.backgroundColor="yellow";
            return false;
            }
            
  if (!document.forms[0].agree.checked) {
      alert ("\n You must agree to the terms of service");
            document.forms[0].agree.focus();
            document.forms[0].agree.style.backgroundColor="yellow";
            return false;
      }   
            
    if (document.forms[0].numfield.value == "")
            {
            alert ("\n You must proove you are a human by entering the sum.");
            document.forms[0].numfield.focus();
            document.forms[0].numfield.style.backgroundColor="yellow";
            return false;
            }
                        
    if (document.forms[0].lastname.value == "")
            {
            alert ("\n Please enter your Last Name.");
            document.forms[0].lastname.focus();
            document.forms[0].lastname.style.backgroundColor="yellow";
            return false;
            }
    if (document.forms[0].firstname.value == "")
            {
            alert ("\n Please enter your First Name.");
            document.forms[0].firstname.focus();
            document.forms[0].firstname.style.backgroundColor="yellow";
            return false;
            }
    if (document.forms[0].class.value == "999")
            {
            alert ("\n Please select your class level.");
            document.forms[0].firstname.focus();
            document.forms[0].firstname.style.backgroundColor="yellow";
            return false;
            }
            
  return true;
           
}

function openTOSWindow() {
  popupWin = window.open('termsofservice.php','open_window',
            ' directories, scrollbars, resizable, dependent, width=640, height=590, left=0, top=0')
 }
</script>

    
</head>

<body class="login"  onload="setDefaultCountry();" >

<?php @include('menu-logged-out.php');?>

<div class="container col-md-12">  
	<div class="row ">
		<div class="col-lg-12 centerme updateArea updateMessage">

		  <div class="changepassword"><h2>Site Registration Form(Active MA Students Only Please!)</h2><span>Back to <a href="login.php">Login</a> Page</span>
    </div>

			<form  action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="form"  name="form" class="forgotForm" role="form" onSubmit="return submitform1();" >
       
		<?php if (!empty($_GET) ) {            			
				
			$strError = '<div class="formerror"><p><img src="images/error.png" width="32" height="32" hspace="5" alt="">Please check the following and try again:</p><ul>';
                 		
          if (!empty($_SESSION['myarray']) )
          {          	
                  foreach ($_SESSION['myarray'] as $error) {
                      $strError .= "<li>$error</li>";
                  }
          }
                  $strError .= '</ul></div>';
          			
          			 echo $strError;
          	
           
        } //if GET
		 
		?> 
          <table id="table1" width="100%" border="0" cellspacing="1" cellpadding="2">
           <tr>
              <td width="30%"><label>Email*</label></td>
              <td width="20%"><input  class="form-control" name="email" type="text" id="email" size="40" maxlength="50" 
              value="<?php if (isset($_GET['email'])) {echo $_GET['email']; }?>"/></td>
               <td width="35%"><span>( Enter your Email address )</span></td>
            </tr>
            <tr>
              <td><label>Email Again*</label></td>
              <td><input  class="form-control" name="email2" type="text" id="email2" size="40" maxlength="50" 
              value="<?php if (isset($_GET['email2'])) {echo $_GET['email2']; }?>"/></td>
            </tr>
           
            <tr>
              <td><label>Password*</label></td>
              <td><input  class="form-control" name="password" type="password" id="password" size="20" maxlength="20" 
              value=""/> 
              </td>
              <td><span>( up to 15 characters )</span></td>
            </tr>
            <tr>
              <td><label>Enter Password Again*</label></td>
              <td><input  class="form-control" name="password2" type="password" id="password2" size="20" maxlength="20"
              value="" /></td>

            </tr>    
            <tr>
              <td><label>Last Name*</label></td>
              <td><input  class="form-control" name="lastname" type="text" id="lastname" size="40" maxlength="50" 
              value="<?php if (isset($_GET['lastname'])) {echo $_GET['lastname']; }?>"/></td>
            </tr>
            <tr>
              <td><label>First Name*</label></td>
              <td><input  class="form-control" name="firstname" type="text" id="firstname" size="40" maxlength="50" 
              value="<?php if (isset($_GET['firstname'])) {echo $_GET['firstname']; }?>"/></td>
            </tr>               
         
             <tr>
              <td><label>Student Level*</label></td>
              <td>
              <select name="class" required >
              <option value="999" >Select a class</option>
              <option value="l1" >Level 1</option>
              <option value="l2">Level 2</option>
              <option value="l3">Level 3</option>
              <option value="l3l">Level 3 Live</option>
              </select>
              </td>
            </tr>  

            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <?php 
				//generate random variables for checksum

				$num1 = rand(1,9);
				$num2 = rand(1,9);
			?>

              <td align="right"><?php echo $num1." + ".$num2." = "?></td>
              <td><input  class="form-control" name="numfield" type="text" id="numfield" size="4" maxlength="2" value="" /></td>
            </tr>
            <tr>
              <td>I agree to the <a href="javascript: openTOSWindow();">Terms of Service</a> Agreement*</td>
              <td><input name="agree" type="checkbox" id="agree" value="checked" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><button type="submit" name="finish" id="finish" class="btn btn-primary" >Register Me</button></td>
            </tr>
          </table>
      	  <input name="num1" type="hidden" value="<?php echo $num1; ?>">
          <input name="num2" type="hidden" value="<?php echo $num2; ?>">
        </form>

		<script type="text/javascript" >
				function checknumfield() { 
					 
					 sum = <?php echo $num1+$num2; ?>;
					 
					 if (window.document.forms[0].numfield.value = 'sum')
					  alert("The two numbers did not add up correctly. Looking for: , " + sum);	 
				}
		</script>
			
		</div> <!-- col-lg-12  centerme-->	
	</div> <!-- row -->	
</div> <!--container -->
<br />

</div> 
<?php require_once('footer.php'); ?>