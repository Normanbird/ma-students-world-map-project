<?php session_start();

require_once('includes/c1.php');
require_once('includes/functions.php');
//require_once('includes/functions.php'); ?>
<?php
// ** Logout the current user. **

$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  $_SESSION = array(); session_destroy();
	
  $logoutGoTo = "login.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!empty($_POST)) {
	$salt = 'alchemy';
	$email = $_POST['email'];
	$email2 = $_POST['email2'];
	$password = $_POST['password'];
	$password = urlencode($password);
	$password2 = $_POST['password2'];
	
	$numfield = $_POST['numfield'];
	$lastname = $_POST['lastname'];
	$firstname = $_POST['firstname'];

  //new fields
  $country = $_POST['country'];
  $city = $_POST['city'];
  $zip  = $_POST['zip'];
  $latitude = $_POST['latitude'];
  $longitude = $_POST['longitude'];
  $class = $_POST['class'];

	
	//read post variables from form into variables using class
	$m = array();
	$user = new processuser;
	//check if username already in use in DB
	if ($user->checkuserexist($_POST)) {		
		$m[] = 'That Email addess is already being used in our system and can not be resued. If it is your Email address, try <a href="forgotpassword.php">resetting the password.</a>' ;
	}
			
	if ($password == "" || strlen($password) > 15 || strlen($password) < 4) {					
			$m[] = "Password length must be between 4 & 15 characters"; 
	}
																
	if ($password2 != $password) {			
			$m[] = "The two passwords entered do not match. Please recheck."; 
	}		
		
	if (!preg_match('/^[_A-z0-9-]+((\.|\+)[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,})$/',$email)) {			
			$m[] = "Please check that you have entered a valid email address"; 
	}	
										
	if ($email2 != $email) {			
			$m[] = "The two email addresses entered do not match. Please recheck."; 									
	}					
										
	if ($_POST['num1'] + $_POST['num2'] != $_POST['numfield']) {			
			$m[] = "You failed the human checker, please enter the sum of the numbers correctly"; 
	}		
															
	if (!isset($_POST['agree'])) {					
			$m[] = "Terms of service agreement must be checked"; 
	}													
	
	if ($lastname == "" || strlen($lastname) > 25 || strlen($lastname) < 2) {					
			$m[] = "Last Name must be between 2 & 25 characters"; 
	}

	if ($firstname == "" || strlen($firstname) > 25 || strlen($firstname) < 2) {					
			$m[] = "First Name must be between 2 & 25 characters"; 
	}


	if ( count($m) > 0 ) { 		
		// there is an error in fields filled out so we are sending user back to form.
		$_SESSION["myarray"] = $m;
		$name = "";
		header("location: ./register.php?email=".$email."&email2=".$email2."&lastname=".$lastname."&firstname=".$firstname);
		echo "header isnt firing line 82 of register.php"; exit;
	}
	//if user didnt exist, we add to db and go to second form
		
	$user->addNewRegisteredUser($email, $password, $lastname, $firstname, $country, $city, $latitude, $longitude, $class, $zip) or die("error adding new user");									
} 

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>MA Students World Map Project - Registration Form</title>
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
 <!-- <script src="js/modernizr.js"></script>-->
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="style.css">

<script src="http://api.geonames.org/export/geonamesData.js?username=databaseindays"></script>
<script src="http://www.geonames.org/export/jsr_class.js"></script>

<script type="text/javascript">
  
  // postalcodes is filled by the JSON callback and used by the mouse event handlers of the suggest box
var postalcodes;

// this function will be called by our JSON callback
// the parameter jData will contain an array with postalcode objects
function getLocation(jData) {
  if (jData == null) {
    // There was a problem parsing search results
    return;
  }

  // save place array in 'postalcodes' to make it accessible from mouse event handlers
  postalcodes = jData.postalcodes;
      
  if (postalcodes.length > 1) {
    // we got several places for the postalcode
    // make suggest box visible
    document.getElementById('suggestBoxElement').style.visibility = 'visible';
    var suggestBoxHTML  = '';
    // iterate over places and build suggest box content
    for (i=0;i< jData.postalcodes.length;i++) {
      // for every postalcode record we create a html div 
      // each div gets an id using the array index for later retrieval 
      // define mouse event handlers to highlight places on mouseover
      // and to select a place on click
      // all events receive the postalcode array index as input parameter
      suggestBoxHTML += "<div class='suggestions' id=pcId" + i + " onmousedown='suggestBoxMouseDown(" + i +")' onmouseover='suggestBoxMouseOver(" +  i +")' onmouseout='suggestBoxMouseOut(" + i +")'> " + postalcodes[i].countryCode + ' ' + postalcodes[i].postalcode + ' &nbsp;&nbsp; ' + postalcodes[i].placeName  +'</div>' ;
    }
    // display suggest box
    document.getElementById('suggestBoxElement').innerHTML = suggestBoxHTML;
  } else {
    if (postalcodes.length == 1) {
      // exactly one place for postalcode
      // directly fill the form, no suggest box required 
      var placeInput = document.getElementById("placeInput");
      var latitude = document.getElementById("latitude");
      var longitude = document.getElementById("longitude");
      placeInput.value = postalcodes[0].placeName;
      latitude.value = postalcodes[0].lat;
      longitude.value = postalcodes[0].lng;
    }
    closeSuggestBox();
  }
}


function closeSuggestBox() {
  document.getElementById('suggestBoxElement').innerHTML = '';
  document.getElementById('suggestBoxElement').style.visibility = 'hidden';
}


// remove highlight on mouse out event
function suggestBoxMouseOut(obj) {
  document.getElementById('pcId'+ obj).className = 'suggestions';
}

// the user has selected a place name from the suggest box
function suggestBoxMouseDown(obj) {
  closeSuggestBox();
  var placeInput = document.getElementById("placeInput");
  var latitude = document.getElementById("latitude");
  var longitude = document.getElementById("longitude");
  placeInput.value = postalcodes[obj].placeName;
  latitude.value = postalcodes[obj].lat;
  longitude.value = postalcodes[obj].lng;
}

// function to highlight places on mouse over event
function suggestBoxMouseOver(obj) {
  document.getElementById('pcId'+ obj).className = 'suggestionMouseOver';
}


// this function is called when the user leaves the postal code input field
// it call the geonames.org JSON webservice to fetch an array of places 
// for the given postal code 
function postalCodeLookup() {

  var country = document.getElementById("countrySelect").value;

  if (geonamesPostalCodeCountries.toString().search(country) == -1) {
     return; // selected country not supported by geonames
  }
  // display loading in suggest box
  document.getElementById('suggestBoxElement').style.visibility = 'visible';
  document.getElementById('suggestBoxElement').innerHTML = '<small><i>loading ...</i></small>';

  var postalcode = document.getElementById("zip").value;

  request = 'http://api.geonames.org/postalCodeLookupJSON?postalcode=' + postalcode  + '&country=' + country  + '&callback=getLocation' + '&style=long&username=databaseindays';

  // Create a new script object
  aObj = new JSONscriptRequest(request);
  // Build the script tag
  aObj.buildScriptTag();
  // Execute (add) the script tag
  aObj.addScriptTag();
}


// set the country of the user's ip (included in geonamesData.js) as selected country 
// in the country select box of the address form
function setDefaultCountry() {
  var countrySelect = document.getElementById("countrySelect");
  for (i=0;i< countrySelect.length;i++) {
    // the javascript geonamesData.js contains the countrycode
    // of the userIp in the variable 'geonamesUserIpCountryCode'
    if (countrySelect[i].value == geonamesUserIpCountryCode) {
      // set the country selectionfield
      countrySelect.selectedIndex = i;
    }
  }
}

function submitform1() 
{ 
   
  if (window.document.forms[0].password.value == "")
            {
            alert ("\n Please enter a password");
            document.forms[0].password.focus();
            document.forms[0].password.style.backgroundColor="yellow";
            return false;
            }
   
  if (window.document.forms[0].password2.value == "")
            {
            alert ("\n Please enter Password again");
            document.forms[0].password2.focus();
            return false;
            }
            
  if (document.forms[0].password.value != document.forms[0].password2.value)
            {
            alert ("\n The Passwords you entered do not Match \n\n Please recheck.");
            document.forms[0].password2.focus();
            return false;
            }
            
  if (document.forms[0].email.value == "")
            {
            alert ("\n Please enter your email address.");
            document.forms[0].email.focus();
            document.forms[0].email.style.backgroundColor="yellow";
            return false;
            }
            
            
  if (document.forms[0].email2.value == "")
            {
            alert ("\n Please enter your email address again.");
            document.forms[0].email2.focus();
            document.forms[0].email2.style.backgroundColor="yellow";
            return false;
            } 
            
  if (document.forms[0].email.value != document.forms[0].email2.value)
            {
            alert ("\n The Emails you entered do not Match \n\n Please enter your email address.");
            document.forms[0].email2.focus();
            document.forms[0].email2.style.backgroundColor="yellow";
            return false;
            }
            
  if (!document.forms[0].agree.checked) {
      alert ("\n You must agree to the terms of service");
            document.forms[0].agree.focus();
            document.forms[0].agree.style.backgroundColor="yellow";
            return false;
      }   
            
    if (document.forms[0].numfield.value == "")
            {
            alert ("\n You must proove you are a human by entering the sum.");
            document.forms[0].numfield.focus();
            document.forms[0].numfield.style.backgroundColor="yellow";
            return false;
            }
                        
    if (document.forms[0].lastname.value == "")
            {
            alert ("\n Please enter your Last Name.");
            document.forms[0].lastname.focus();
            document.forms[0].lastname.style.backgroundColor="yellow";
            return false;
            }
    if (document.forms[0].firstname.value == "")
            {
            alert ("\n Please enter your First Name.");
            document.forms[0].firstname.focus();
            document.forms[0].firstname.style.backgroundColor="yellow";
            return false;
            }
            
  return true;
           
}

function openTOSWindow() {
  popupWin = window.open('termsofservice.php','open_window',
            ' directories, scrollbars, resizable, dependent, width=640, height=590, left=0, top=0')
 }
</script>

    
</head>

<body class="login"  onload="setDefaultCountry();" >

<?php @include('menu-logged-out.php');?>

<div class="container col-md-12">  
	<div class="row ">
		<div class="col-lg-12 centerme updateArea updateMessage">

		  <div class="changepassword"><h2>New Member Registration Form</h2><span>Back to <a href="login.php">Login</a> Page</span>
    </div>

			<form  action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="form"  name="form" class="forgotForm" role="form" onSubmit="return submitform1();" >
       
		<?php if (!empty($_GET) ) {            			
				
			$strError = '<div class="formerror"><p><img src="images/error.png" width="32" height="32" hspace="5" alt="">Please check the following and try again:</p><ul>';
                 		
          if (!empty($_SESSION['myarray']) )
          {          	
                  foreach ($_SESSION['myarray'] as $error) {
                      $strError .= "<li>$error</li>";
                  }
          }
                  $strError .= '</ul></div>';
          			
          			 echo $strError;
          	
           
        } //if GET
		 
		?> 
          <table id="table1" width="100%" border="0" cellspacing="1" cellpadding="2">
           <tr>
              <td width="30%"><label>Email*</label></td>
              <td width="20%"><input  class="form-control" name="email" type="text" id="email" size="40" maxlength="50" 
              value="<?php if (isset($_GET['email'])) {echo $_GET['email']; }?>"/></td>
               <td width="35%"><span>( Enter your Email address )</span></td>
            </tr>
            <tr>
              <td><label>Email Again*</label></td>
              <td><input  class="form-control" name="email2" type="text" id="email2" size="40" maxlength="50" 
              value="<?php if (isset($_GET['email2'])) {echo $_GET['email2']; }?>"/></td>
            </tr>
           
            <tr>
              <td><label>Password*</label></td>
              <td><input  class="form-control" name="password" type="password" id="password" size="20" maxlength="20" 
              value=""/> 
              </td>
              <td><span>( up to 15 characters )</span></td>
            </tr>
            <tr>
              <td><label>Enter Password Again*</label></td>
              <td><input  class="form-control" name="password2" type="password" id="password2" size="20" maxlength="20"
              value="" /></td>

            </tr>    
            <tr>
              <td><label>Last Name*</label></td>
              <td><input  class="form-control" name="lastname" type="text" id="lastname" size="40" maxlength="50" 
              value="<?php if (isset($_GET['lastname'])) {echo $_GET['lastname']; }?>"/></td>
            </tr>
            <tr>
              <td><label>First Name*</label></td>
              <td><input  class="form-control" name="firstname" type="text" id="firstname" size="40" maxlength="50" 
              value="<?php if (isset($_GET['firstname'])) {echo $_GET['firstname']; }?>"/></td>
            </tr>  
             
            <tr>
              <td><label>Country*</label></td>
              <td>
                <select id="countrySelect" name="country"><option value="AD"> Andorra</option><option value="AE"> United Arab Emirates</option><option value="AF"> Afghanistan</option><option value="AG"> Antigua and Barbuda</option><option value="AI"> Anguilla</option><option value="AL"> Albania</option><option value="AM"> Armenia</option><option value="AN"> Netherlands Antilles</option><option value="AO"> Angola</option><option value="AQ"> Antarctica</option><option value="AR"> Argentina</option><option value="AS"> American Samoa</option><option value="AT"> Austria</option><option value="AU"> Australia</option><option value="AW"> Aruba</option><option value="AX"> Aland Islands</option><option value="AZ"> Azerbaijan</option><option value="BA"> Bosnia and Herzegovina</option><option value="BB"> Barbados</option><option value="BD"> Bangladesh</option><option value="BE"> Belgium</option><option value="BF"> Burkina Faso</option><option value="BG"> Bulgaria</option><option value="BH"> Bahrain</option><option value="BI"> Burundi</option><option value="BJ"> Benin</option><option value="BM"> Bermuda</option><option value="BN"> Brunei</option><option value="BO"> Bolivia</option><option value="BR"> Brazil</option><option value="BS"> Bahamas</option><option value="BT"> Bhutan</option><option value="BV"> Bouvet Island</option><option value="BW"> Botswana</option><option value="BY"> Belarus</option><option value="BZ"> Belize</option><option value="CA"> Canada</option><option value="CC"> Cocos Islands</option><option value="CD"> The Democratic Republic Of Congo</option><option value="CF"> Central African Republic</option><option value="CG"> Congo</option><option value="CH"> Switzerland</option><option value="CI"> C&ocirc;te d'Ivoire</option><option value="CK"> Cook Islands</option><option value="CL"> Chile</option><option value="CM"> Cameroon</option><option value="CN"> China</option><option value="CO"> Colombia</option><option value="CR"> Costa Rica</option><option value="CS"> Serbia and Montenegro</option><option value="CU"> Cuba</option><option value="CV"> Cape Verde</option><option value="CX"> Christmas Island</option><option value="CY"> Cyprus</option><option value="CZ"> Czech Republic</option><option value="DE"> Germany</option><option value="DJ"> Djibouti</option><option value="DK"> Denmark</option><option value="DM"> Dominica</option><option value="DO"> Dominican Republic</option><option value="DZ"> Algeria</option><option value="EC"> Ecuador</option><option value="EE"> Estonia</option><option value="EG"> Egypt</option><option value="EH"> Western Sahara</option><option value="ER"> Eritrea</option><option value="ES"> Spain</option><option value="ET"> Ethiopia</option><option value="FI"> Finland</option><option value="FJ"> Fiji</option><option value="FK"> Falkland Islands</option><option value="FM"> Micronesia</option><option value="FO"> Faroe Islands</option><option value="FR"> France</option><option value="GA"> Gabon</option><option value="GB"> United Kingdom</option><option value="GD"> Grenada</option><option value="GE"> Georgia</option><option value="GF"> French Guiana</option><option value="GH"> Ghana</option><option value="GI"> Gibraltar</option><option value="GL"> Greenland</option><option value="GM"> Gambia</option><option value="GN"> Guinea</option><option value="GP"> Guadeloupe</option><option value="GQ"> Equatorial Guinea</option><option value="GR"> Greece</option><option value="GS"> South Georgia And The South Sandwich Islands</option><option value="GT"> Guatemala</option><option value="GU"> Guam</option><option value="GW"> Guinea-Bissau</option><option value="GY"> Guyana</option><option value="HK"> Hong Kong</option><option value="HM"> Heard Island And McDonald Islands</option><option value="HN"> Honduras</option><option value="HR"> Croatia</option><option value="HT"> Haiti</option><option value="HU"> Hungary</option><option value="ID"> Indonesia</option><option value="IE"> Ireland</option><option value="IL"> Israel</option><option value="IN"> India</option><option value="IO"> British Indian Ocean Territory</option><option value="IQ"> Iraq</option><option value="IR"> Iran</option><option value="IS"> Iceland</option><option value="IT"> Italy</option><option value="JM"> Jamaica</option><option value="JO"> Jordan</option><option value="JP"> Japan</option><option value="KE"> Kenya</option><option value="KG"> Kyrgyzstan</option><option value="KH"> Cambodia</option><option value="KI"> Kiribati</option><option value="KM"> Comoros</option><option value="KN"> Saint Kitts And Nevis</option><option value="KP"> North Korea</option><option value="KR"> South Korea</option><option value="KW"> Kuwait</option><option value="KY"> Cayman Islands</option><option value="KZ"> Kazakhstan</option><option value="LA"> Laos</option><option value="LB"> Lebanon</option><option value="LC"> Saint Lucia</option><option value="LI"> Liechtenstein</option><option value="LK"> Sri Lanka</option><option value="LR"> Liberia</option><option value="LS"> Lesotho</option><option value="LT"> Lithuania</option><option value="LU"> Luxembourg</option><option value="LV"> Latvia</option><option value="LY"> Libya</option><option value="MA"> Morocco</option><option value="MC"> Monaco</option><option value="MD"> Moldova</option><option value="MG"> Madagascar</option><option value="MH"> Marshall Islands</option><option value="MK"> Macedonia</option><option value="ML"> Mali</option><option value="MM"> Myanmar</option><option value="MN"> Mongolia</option><option value="MO"> Macao</option><option value="MP"> Northern Mariana Islands</option><option value="MQ"> Martinique</option><option value="MR"> Mauritania</option><option value="MS"> Montserrat</option><option value="MT"> Malta</option><option value="MU"> Mauritius</option><option value="MV"> Maldives</option><option value="MW"> Malawi</option><option value="MX"> Mexico</option><option value="MY"> Malaysia</option><option value="MZ"> Mozambique</option><option value="NA"> Namibia</option><option value="NC"> New Caledonia</option><option value="NE"> Niger</option><option value="NF"> Norfolk Island</option><option value="NG"> Nigeria</option><option value="NI"> Nicaragua</option><option value="NL"> Netherlands</option><option value="NO"> Norway</option><option value="NP"> Nepal</option><option value="NR"> Nauru</option><option value="NU"> Niue</option><option value="NZ"> New Zealand</option><option value="OM"> Oman</option><option value="PA"> Panama</option><option value="PE"> Peru</option><option value="PF"> French Polynesia</option><option value="PG"> Papua New Guinea</option><option value="PH"> Philippines</option><option value="PK"> Pakistan</option><option value="PL"> Poland</option><option value="PM"> Saint Pierre And Miquelon</option><option value="PN"> Pitcairn</option><option value="PR"> Puerto Rico</option><option value="PS"> Palestine</option><option value="PT"> Portugal</option><option value="PW"> Palau</option><option value="PY"> Paraguay</option><option value="QA"> Qatar</option><option value="RE"> Reunion</option><option value="RO"> Romania</option><option value="RU"> Russia</option><option value="RW"> Rwanda</option><option value="SA"> Saudi Arabia</option><option value="SB"> Solomon Islands</option><option value="SC"> Seychelles</option><option value="SD"> Sudan</option><option value="SE"> Sweden</option><option value="SG"> Singapore</option><option value="SH"> Saint Helena</option><option value="SI"> Slovenia</option><option value="SJ"> Svalbard And Jan Mayen</option><option value="SK"> Slovakia</option><option value="SL"> Sierra Leone</option><option value="SM"> San Marino</option><option value="SN"> Senegal</option><option value="SO"> Somalia</option><option value="SR"> Suriname</option><option value="ST"> Sao Tome And Principe</option><option value="SV"> El Salvador</option><option value="SY"> Syria</option><option value="SZ"> Swaziland</option><option value="TC"> Turks And Caicos Islands</option><option value="TD"> Chad</option><option value="TF"> French Southern Territories</option><option value="TG"> Togo</option><option value="TH"> Thailand</option><option value="TJ"> Tajikistan</option><option value="TK"> Tokelau</option><option value="TL"> Timor-Leste</option><option value="TM"> Turkmenistan</option><option value="TN"> Tunisia</option><option value="TO"> Tonga</option><option value="TR"> Turkey</option><option value="TT"> Trinidad and Tobago</option><option value="TV"> Tuvalu</option><option value="TW"> Taiwan</option><option value="TZ"> Tanzania</option><option value="UA"> Ukraine</option><option value="UG"> Uganda</option><option value="UM"> United States Minor Outlying Islands</option><option value="US"> United States</option><option value="UY"> Uruguay</option><option value="UZ"> Uzbekistan</option><option value="VA"> Vatican</option><option value="VC"> Saint Vincent And The Grenadines</option><option value="VE"> Venezuela</option><option value="VG"> British Virgin Islands</option><option value="VI"> U.S. Virgin Islands</option><option value="VN"> Vietnam</option><option value="VU"> Vanuatu</option><option value="WF"> Wallis And Futuna</option><option value="WS"> Samoa</option><option value="YE"> Yemen</option><option value="YT"> Mayotte</option><option value="ZA"> South Africa</option><option value="ZM"> Zambia</option><option value="ZW"> Zimbabwe</option></select>
              </td>
            </tr>  
             <tr>
              <td><label>Zip/Postal Code*</label></td>
              <td><input  class="form-control" name="zip" type="text" id="zip" size="15" maxlength="15" onblur="postalCodeLookup();" value="<?php if (isset($_GET['zip'])) {echo $_GET['zip']; }?>"/>
              </td>
              
             </tr>  
              <tr>
              <td><label>City*</label></td>
              <td><input  class="form-control" name="city" type="text" id="placeInput" onblur="closeSuggestBox();"  size="40" maxlength="50" 
              value="<?php if (isset($_GET['city'])) {echo $_GET['city']; }?>"/>
                <div style="visibility: hidden;" id="suggestBoxElement"></div>
              </td>
            </tr>  
            <tr>
              <td><label>Latitude</label></td>
              <td><input  class="form-control" name="latitude" type="text" id="latitude" size="40" maxlength="40" 
              value="<?php if (isset($_GET['latitude'])) {echo $_GET['latitude']; }?>"/></td>
            </tr>  
             <tr>
              <td><label>Longitude</label></td>
              <td><input  class="form-control" name="longitude" type="text" id="longitude" size="40" maxlength="40" 
              value="<?php if (isset($_GET['longitude'])) {echo $_GET['longitude']; }?>"/></td>
            </tr>  
             <tr>
              <td><label>Class*</label></td>
              <td>
              <select name="class" required >
              <option value="999" >Select a class</option>
              <option value="l2">Level 2</option>
              <option value="l3">Level 3</option>
              <option value="l3l">Level 3 Live</option>
              </select>
              </td>
            </tr>  

            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <?php 
				//generate random variables for checksum

				$num1 = rand(1,9);
				$num2 = rand(1,9);
			?>

              <td align="right"><?php echo $num1." + ".$num2." = "?></td>
              <td><input  class="form-control" name="numfield" type="text" id="numfield" size="4" maxlength="2" value="" /></td>
            </tr>
            <tr>
              <td>I agree to the <a href="javascript: openTOSWindow();">Terms of Service</a> Agreement*</td>
              <td><input name="agree" type="checkbox" id="agree" value="checked" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><button type="submit" name="finish" id="finish" class="btn btn-primary" >Register Me</button></td>
            </tr>
          </table>
      	  <input name="num1" type="hidden" value="<?php echo $num1; ?>">
          <input name="num2" type="hidden" value="<?php echo $num2; ?>">
        </form>

		<script type="text/javascript" >
				function checknumfield() { 
					 
					 sum = <?php echo $num1+$num2; ?>;
					 
					 if (window.document.forms[0].numfield.value = 'sum')
					  alert("The two numbers did not add up correctly. Looking for: , " + sum);	 
				}
		</script>
			
		</div> <!-- col-lg-12  centerme-->	
	</div> <!-- row -->	
</div> <!--container -->
<br />

</div> 
<?php require_once('footer.php'); ?>