<?php require_once('includes/c1.php'); 
require_once('includes/functions.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple markers</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>


function initialize() {


var centerLang = new google.maps.LatLng(40.023438,-82.045634);

//info for required map options
var mapOptions = {
    zoom: 3,
    center: centerLang
    
  }

  //create our map object using options
var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

<?php $r = getMarkers(); 
while ($marker = mysqli_fetch_assoc($r))
    { ?>
       <?php if ( getIcon($marker['class']) == true) { ?>

         var image = 'images/l3l.png';

       <?php  } else  ?>
           var image = 'images/l3.png';
       
      var myLatlng_<?php echo $marker['markerid']; ?> = new google.maps.LatLng(<?php echo $marker['lat']; ?>,<?php echo $marker['long']; ?>);

      var marker_<?php echo $marker['markerid']; ?>   = new google.maps.Marker({
          position: myLatlng_<?php echo $marker['markerid']; ?>,
          map: map,
          title: '<?php echo $marker['desc']; ?>',
          icon: image
      }); 

  <?php }  ?>



}//initialize

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
  
    <div id="map-canvas"></div>
  </body>
</html>