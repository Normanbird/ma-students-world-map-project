<?php
require_once('includes/init.php');

if (isset($_POST['id'])) {
  $id = $_POST['id'];
  $email = $_POST['email'];  
  $password = $_POST['password'];
  $password = urlencode($password);  
  $lastname = $_POST['lastname'];
  $firstname = $_POST['firstname'];

  //new fields
  // $country = $_POST['country'];
  // $city = $_POST['city'];
  // $zip  = $_POST['zip'];
  // $latitude = $_POST['latitude'];
  // $longitude = $_POST['longitude'];
  $class = $_POST['class'];

  
  //read post variables from form into variables using class
  $m = array();
  $user = new processuser;
  //check if username already in use in DB
  if ($email != $_SESSION['UserEmail'] ){ 
        if ($user->checkuserexist($_POST['email']) ) {    
          $m[] = 'That Email addess is already being used in our system and can not be resued. You must enter a different Email.';
        }
  }
      
  if ($password == "" || strlen($password) > 15 || strlen($password) < 4) {         
      $m[] = "Password length must be between 4 & 15 characters"; 
  }                         
  
    
  if (!preg_match('/^[_A-z0-9-]+((\.|\+)[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,})$/',$email)) {      
      $m[] = "Please check that you have entered a valid email address"; 
  }         
  
  if ($lastname == "" || strlen($lastname) > 25 || strlen($lastname) < 2) {         
      $m[] = "Last Name must be between 2 & 25 characters"; 
  }

  if ($firstname == "" || strlen($firstname) > 25 || strlen($firstname) < 2) {          
      $m[] = "First Name must be between 2 & 25 characters"; 
  }


  if ( count($m) > 0 ) {    
    // there is an error in fields filled out so we are sending user back to form.
    $_SESSION["myarray"] = $m;
    $name = "";
    header("location: ./profile.php?email=".$email."&email2=".$email2."&lastname=".$lastname."&firstname=".$firstname);
   
  }
  //if user didnt exist, we add to db and go to second form
    
  //update member info into DB

  //hashes the password prior to placing in DB
  $password = bcrypt_hash($password, $complexity = 12);

  $insertsql = "UPDATE members SET memberemail = ?, memberpassword=?, lastname=?, firstname=?, class=? WHERE memberid = ? ";
      /* Prepare statement */
      $stmt = $c1->prepare($insertsql);
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $insertsql . ' Error: ' . $c1->error, E_USER_ERROR);
      }
      /* Bind the 42 parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      $stmt->bind_param('sssssi', $email, $password, $lastname, $firstname, $class, $id);       
      /* Execute statement */
      $stmt->execute();

      header('location: ./index.php'); 
              


} 

?>