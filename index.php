<?php require_once 'includes/init.php';  ?>
<!DOCTYPE html>
<html>
  <head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Mastering Alchemy Students Terrestrial Earth Regions System</title>
    <meta name="description" content="Update location in profile of Alchemy Student's. Spirituality" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">    
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>


<script language="Javascript">

function initialize() {

var centerLang = new google.maps.LatLng(23.546813,12.753906);

//info for required map options
var mapOptions = {
    zoom: 3,
    maxZoom: 14,
    center: centerLang,
    streetViewControl: false
    
  }

  //create our map object using options
var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


<?php $r = getMarkers(); 

while ($marker = mysqli_fetch_assoc($r))
    { 
      $description = $marker['firstname']. ' '. $marker['lastname'];
      ?>  
      
      // limit zoom level
        // var minZoomLevel = 5;

      var myLatlng_<?php echo $marker['memberid']; ?> = new google.maps.LatLng(<?php echo $marker['latitude']; ?>,<?php echo $marker['longitude']; ?>);

      var marker_<?php echo $marker['memberid']; ?>   = new google.maps.Marker({
          position: myLatlng_<?php echo $marker['memberid']; ?>,
          map: map,
          title: '<?php echo $description; ?>',
          icon: "<?php echo getIcon($marker['class']); ?>"
      }); 

  <?php }  ?>



}//initialize

google.maps.event.addDomListener(window, 'load', initialize);

</script>

</head>
  <body>
  <?php include('menu.php'); ?>
   
    <div id="map-canvas"></div>


 <?php require_once('footer.php') ?>  