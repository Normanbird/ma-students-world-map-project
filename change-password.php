<?php
require_once('includes/init.php');


if (isset($_POST['id'])) {
    //get id from session 
  $password = $_POST['password'];
  $password = urlencode($password);  
  $id = $_POST['id'];
  
  //read post variables from form into variables using class
  $m = array();
   //check if username already in use in DB
       
  if ($password == "" || strlen($password) > 15 || strlen($password) < 4) {         
      $m[] = "Password length must be between 4 & 15 characters"; 
  }                         
  
   if ( count($m) > 0 ) {    
    // there is an error in fields filled out so we are sending user back to form.
    $_SESSION["myarray"] = $m;
    $name = "";
    header("location: ./change-password.php?error=true");
   
  }
  //if user didnt exist, we add to db and go to second form
    
  //update member info into DB

  //hashes the password prior to placing in DB
  $password = bcrypt_hash($password, $complexity = 12);

  $updatesql = "UPDATE members SET memberpassword=? WHERE memberid = ? ";
      /* Prepare statement */
      $stmt = $c1->prepare($updatesql);
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $updatesql . ' Error: ' . $c1->error, E_USER_ERROR);
      }
      /* Bind the 42 parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      $stmt->bind_param('si', $password, $id);       
      /* Execute statement */
      $stmt->execute();

      header('location: ./profile.php');              


} 

?>

 <!-- lets display form in edit mode for admin user -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="style.css">
    <script type="text/javascript">


function submitform1() 
{    
  if (window.document.forms[0].password.value == "")
            {
            alert ("\n Please enter a password");
            document.forms[0].password.focus();
            document.forms[0].password.style.backgroundColor="yellow";
            return false;
            } 
            
  return true;           
}
</script>	
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	   
    <?php require_once('menu.php'); ?>

       <!-- start of container page -->

<div class="container col-md-12">  
  <div class="row ">
    <div class="col-lg-12 ">

      <h2 class="profile">Change Your Password</h2>
        <ul class="submenu">
               <li><a href="profile.php">Edit Profile</a></li>
               <li><a href="update-location.php">update location</a></li>
        </ul>   

      <form  action="<?php $_SERVER['PHP_SELF'] ?>" method="post"  name="passwordform" role="form" onSubmit="return submitform1();" >
       
    <?php if (!empty($_GET) ) {                 
        
      $strError = '<div class="formerror"><p><img src="images/error.png" width="32" height="32" hspace="5" alt="">Please check the following and try again:</p><ul>';
                    
          if (!empty($_SESSION['myarray']) )
          {           
                  foreach ($_SESSION['myarray'] as $error) {
                      $strError .= "<li>$error</li>";
                  }
          }
                  $strError .= '</ul></div>';
                
                 echo $strError;
            
           
        } //if GET
     
    ?> 
          <table id="table1" width="100%" border="0" cellspacing="1" cellpadding="2">
          
            <tr>
              <td><label>Password*</label></td>
              <td><input  class="form-control" name="password" type="password" id="password" size="20" maxlength="20" 
              value=""/> 
              </td>
              <td><span>( up to 15 characters )</span></td>
            </tr>
             
           <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          
            <tr>
              <td>&nbsp;</td>
              <td><button type="submit" name="finish" id="finish" class="btn btn-primary" >Update Profile</button></td>
            </tr>
          </table>
          <input type="hidden" name="id" value="<?php echo $_SESSION['memberid']; ?>">
        </form>

         
    </div> <!-- col-lg-12  centerme-->  
  </div> <!-- row --> 
</div> <!--container -->
<br />

        <?php require('footer.php') ?>     