<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <script src="js/modernizr.js"></script>
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="style.css">
  
  
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="privacypolicy">
<table style="width:auto; margin: 0 auto; padding-bottom:20px; ">
    <tr>
       <td>
       <h1 >MA Students World Map Project Privacy Policy: </h1></td></tr>
    <tr>
       <td >
       <p><em>Use of the MA Students World Map Project is governed by this MA Students World Map Project Privacy Policy. In order to access the site, you will need to acknowledge that you have read, understand, and will abide by this policy.</em> </p> 

       <p><strong>Privacy Policy </strong> MA Students World Map Project</p>
<p>The MA Students World Map Project privacy policy is simple: we do not sell, rent or give away our database of personal information, nor do we let anyone use it for any form of commercial advertising. It is only used for the volunteer members of this site.</p>

<p>The database includes mailing addresses, telephone numbers, email addresses and information provided during registration. We collect this information for this online applications use only.</p> 

<p>By registering, you agree not to share any information you see or access, with anyone outside of this site. </p>

<p>You may be able to email another student using the system. If able to do so, you agree not to spam or bug any other students using the system.</p> 

<p>We include email addresses and physical address in our database for three reasons.<br>
1.) To provide access to the system via an email address and password.<br>
2.) When we need to inform registered students of a system related event or information.<br>
3.) Plot your location on the map.</p>
<br>
<p>By registering and using this MA Students World Map Project site you are stating: <em>"I have read, understand and will abide by the MA Students World Map Project Privacy Policy".
</em></p> 



      </td>
   </tr><tr><td>&nbsp;</td></tr>
</table>

</body>
</html>