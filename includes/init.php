<?php 
session_start();

require_once('includes/c1.php');
require_once('includes/functions.php');

date_default_timezone_set('US/Eastern');
//$currenttime = time();
 
// $GLOBALS['config'] = array(
// 	'mysqli' => array(
// 		'hostname' => '127.0.0.1',
// 		'username' => 'root',
// 		'password' => '',
// 		'db' => 'mvyc'
// 	),
// 	'remember' => array(
// 		'cookie_name' => 'hash',
// 		'cookie_expiry' => '604800',
// 		'password' => '',
// 		'db' => 'mvyc'
// 	),
// 	'session' => array(
// 		'session_name' => 'user'
// 	)

// );


//logout stuff
// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION = array(); session_destroy();
	
  $logoutGoTo = "login.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}//end if isset doLogout


$did_restrictGoto = "login.php";
if (!isset($_SESSION['UserEmail'])) {  

  $did_qsChar = "?";
  $did_referrer = $_SERVER['PHP_SELF'];
  if (strpos($did_restrictGoto, "?")) $did_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $did_referrer .= "?" . $QUERY_STRING;
  $did_restrictGoto = $did_restrictGoto. $did_qsChar . "accesscheck=" . urlencode($did_referrer);
  header("Location: ". $did_restrictGoto); 
  exit;
}
?>