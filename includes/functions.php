<?php 
require_once('includes/c1.php');
//constants used in app
define('TO_EMAIL', 'info@databaseindays.com');
define('FROM_EMAIL', 'info@alchemymap.com');
  // create email headers
define('HEADERS', 'From: '.FROM_EMAIL."\r\n".'Reply-To: '.FROM_EMAIL."\r\n" .'X-Mailer: PHP/' . phpversion());

// class to connect to db and other things for online journal
//author norman bird
//date 8/23/2011

//require_once "connections/c1.php";
class processuser {
  
  var $name; 
  var $password; 
  var $email ;
  var $gender;
   
  var $user = array();



//*************************************************************************************************************************************   
function lastupdated() {
  global $c1;
  $user = $_SESSION['UserEmail'];
$sql = "UPDATE members SET last_login = NOW() WHERE memberemail = '$user' ";

if (!$c1->query($sql))
        { die('Error: updating lastupdated time' . mysql_error());
      } else { 
      // code to update last update time for user
      return true;} 
}

//*************************************************************************************************************************************   
function newregmail($lastname, $firstname) {

 $subject = "A New MA Studentr has registered on MA Students World Map Project";
 $body = "Hi,\n\nA new member just registered on The MA Students World Map Project\n\nUser Name: ". $firstname. " ".$lastname. "\n\nEmail: ".$_SESSION['UserEmail'];
 
 if (mail(TO_EMAIL, $subject, $body, HEADERS)) { return true;} else {return false;}

} // end newregmail


//****************************************************************************************************************************
function newloginmail() {
  $user = $_SESSION['UserEmail'];
  if ($user == 'norman144' ) {
    return true;
  } else {
    
     
     $subject = $user." just logged back in on MA Students Map Project Site. Wohoooo!";
     $body = "Hi,\n\nA member just logged back in on MA Students Map Project Site\n\nUser Email: ".$user;
      if (mail(TO_EMAIL, $subject, $body, HEADERS)) {
       return true;}
         else { return false;  }
      }
} // end newregmail

//*******************************************************************************************************************************  
function loguser() {
  global $c1;

  //when user logs in , lets add an entry to the log file..
    $user = $_SESSION['UserEmail'];    
    $ip = $_SERVER['REMOTE_ADDR'];    
    $useragent = $_SERVER['HTTP_USER_AGENT'];                  
  
  $sql = "INSERT INTO log ( user, ip, useragent) VALUES
  ('$user','$ip', '$useragent')";
  if (!$c1->query($sql))
        { die('Error: inserting into profile table, functions ' . mysql_error());
      } else { 
      // code to update last update time for user
      
      $userlastupdate = new processuser();
        $userlastupdate->lastupdated();
        // $userlastupdate->newloginmail();
      return true;}

} // end loguser
  

//***************************************************************************************************************************
function checkuserexist($user) {
  global $c1;
$this->name = $user['email']; 

addslashes($this->name);

$result = $c1->query("SELECT * FROM members WHERE memberemail ='$this->name'");

if (mysqli_num_rows($result) == 1)
  {
     return true;
  }   

    return false;
} //end checkuserexist //***************************************************************************************************************************



function addNewRegisteredUser($email, $password, $lastname, $firstname, $class) { 
 global $c1;
 $active = 1;
  //we are going to add her to database and then send to second part of registration.
  $_SESSION['UserEmail'] = $email;
  $_SESSION['password'] = $password;       
  
  //hashes the password prior to placing in DB
  $password = bcrypt_hash($password, $complexity = 12);

  $insertsql = "INSERT INTO members (memberemail, memberpassword, lastname, firstname, create_date, active, class) VALUES (?, ?, ?, ?, NOW(), ?, ?)";
      /* Prepare statement */
      $stmt = $c1->prepare($insertsql);
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $insertsql . ' Error: ' . $c1->error, E_USER_ERROR);
      }
      /* Bind the 42 parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      $stmt->bind_param('ssssis', $email, $password, $lastname, $firstname, $active, $class);       
      /* Execute statement */
      $stmt->execute();

         $user = new processuser();
         // send registration email ot admin
        // if (!$user->newregmail($lastname, $firstname)) echo "notification email fail";
        // send mail in future, now just send user to main page of journal

         // lets obtain member id and set session variable accordingly
              $sql = "select * from members WHERE memberemail='$email' limit 1";
              
              if ($results = $c1->query($sql))              
               {
                  $row = $results->fetch_assoc();  
                  $_SESSION["UserEmail"] = $row["memberemail"]; 
                  $_SESSION["memberid"] = $row["memberid"];            
                  $_SESSION['lookup'] = true;

                  //set session variable to alert lookup.php that we are coming its way. 
                 
                  header('location: ./lookup.php'); 
              }//end else for select *     
              else { trigger_error('Wrong SQL: ' . $insertsql . ' Error: ' . $c1->error, E_USER_ERROR); }    

return false;

 }//end addnewRegisteredUser
 
 

 function addnewuser($user) { 
 global $c1;

  //we are going to add her to database and then send to second part of registration.
  $_SESSION['UserEmail'] = $email = mysqli_escape_string($c1,$user['email']);
  $_SESSION['password'] = $password = mysqli_escape_string($c1,$user['password']);
        
  
  //hashes the password prior to placing in DB
  $password = bcrypt_hash($password, $complexity = 12);


  $sql = "INSERT INTO members ( memberemail, memberpassword, create_date) VALUES
  ('$email', '$password', NOW())";
  //run insert query
  if (!$c1->query($sql))
        { die('Error: inserting New User into user table, functions ' . mysql_error());
      }else{
         $user = new processuser();
         //send registration email ot admin
        if (!$user->newregmail()) echo "notification email fail";  
        //send mail in future, now just send user to main page of journal
        header("location: index.php");        
           }  

return false;

 }//end addnewuser
 


}//end class processuser



//***************************************************************************************************************************
function getIcon($c) {  


if ($c == 'l3l' ) {  return 'images/l3l.png';  }   
if ($c == 'l3' )  {  return 'images/l3.png';  } 
if ($c == 'l2' )  {  return 'images/l2.png';   } 
if ($c == 'l1' )  {  return 'images/l1.png';   } 


} //end getIcon //***************************************************************************************************************************


//***************************************************************************************************************************
function getMarkers() {
  global $c1;

$results = $c1->query("SELECT * FROM members");

if (mysqli_num_rows($results) > 0 )
  {
    
     return $results;
  }   

    return false;
} //end getMarkers//***************************************************************************************************************************


function changePasswordEmail($password) {
if (isset($_SESSION['UserEmail'])) {
  $to = $_SESSION['UserEmail'];

}
 $subject = "Password Change Notification";
 $body = "Hi,\n\nThis is to inform you that your password on The MA Students Map Project Site has been changed to\n\nNew Password: ". $password;
 
 if (mail($to, $subject, $body, HEADERS)) { return true;} else {return false;}


}
/*****************************************************************************************************************
 * Hash a string using bcrypt with specified complexity
 *
 * @param  string $password input string
 * @param  integer $complexity bcrypt exponential cost
 * @return string
 */
function bcrypt_hash($password, $complexity = 12)
{
  if ($complexity < 4 || $complexity > 31) {
    throw new InvalidArgumentException("BCrypt complexity must be between 4 and 31");
  }
 
  // CRYPT_BLOWFISH salts must be 22 alphanumeric characters long
  $random = get_random_alnum_salt(22);
 
  // The crypt function decides which algorithm to use (we need Blowfish) based on
  // the format of the salt parameter
  $salt = sprintf('$2a$%02d$%s', $complexity, $random);
 
  return crypt($password, $salt);
}
 
/**
 * This generates a random alphanumeric string of length $length.
 *
 * This may not be a cryptographic grade random string generation
 * function - but it is good enough for our example
 *
 * @param  integer $length random string length
 * @return string
 */
function get_random_alnum_salt($length)
{
  static $chars = null;
  if (! $chars) {
    $chars = implode('', array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9)));
  }
 
  $salt = '';
  for ($i = 0; $i < $length; $i++) {
    $salt .= $chars[mt_rand(0, 61)];
  }
  return $salt;
}
 
/**
 * Check a password against a hashed string
 *
 * @param  string $password cleartext password
 * @param  string $hashed bcrypt format hashed string
 * @return boolean
 */
function bcrypt_check_hash($password, $hashed)
{
  // Do some quick validation that $hashed is indeed a bcrypt hash
  if (strlen($hashed) != 60 || ! preg_match('/^\$2a\$\d{2}\$/', $hashed)) {
    //echo "the variable hashed contains: ". $hashed;
    throw new InvalidArgumentException("Provided hash is not a bcrypt string hash");
  }
  return (crypt($password, $hashed) === $hashed);
}


function generate_new_password($email) {
global $c1;
// Generate a new 7-character password
$password = substr(md5(uniqid(rand())),0,7);
//hash it with bcrypt for the database
$hashed_password = bcrypt_hash($password, $complexity = 4);

// Insert into the db
$statement = "UPDATE members SET memberpassword = '$hashed_password' WHERE memberemail = '$email'";
if ( !$c1->query($statement)) {
        
    die('Error: while generating the new password' . mysql_error()); 
    }

$message = "Dear $email,\r\n\r\n";
$message .= "A request has been made to reset your password on MA Students Map Project Site\r\n";
$message .= "Your password has been reset.\r\n";
$message .= "Your Username is your email: $email.\r\n\r\n";

$message .= "Your new password is:\r\n";
$message .= "---------------------------\r\n";
$message .= "        $password      \r\n";
$message .= "---------------------------\r\n\r\n";

$message .= "Please use the new password above for future logins to MA Students Map Project Site\r\n\r\n";
$message .= "Thank you.\r\n";
$message .= "MA Students World Map Project\r\n\r\n";

$email_to = $email;
$email_subject = "Password reset from MA Students World Map Project Website";


// create email headers
$headers = 'From: '.FROM_EMAIL."\r\n".
'Reply-To: '.FROM_EMAIL."\r\n" .
'X-Mailer: PHP/' . phpversion();

// E-mail new password to user
mail($email,$email_subject,$message,$headers);

}//end generate new password


function set_session_vars() {
    $nb_args=func_num_args();
    $arg_list=func_get_args();
    for($i=0;$i<$nb_args;$i++) {
        global $$arg_list[$i];
        $_SESSION[$arg_list[$i]] = $$arg_list[$i];
    }
}

function deleteFamily($memberid, $fname, $frelation, $fmonth, $fyear) {
//delete fields from family table that match memberid
   include 'includes/c1.php'; 

//first delete entries belonging to memberid
$deletesql = "DELETE FROM family where memberid= ? limit 8";
       /* Prepare statement */
      $stmt = $c1->prepare($deletesql);
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $deletesql . ' Error: ' . $c1->error, E_USER_ERROR);
      }       
      /* Bind the  parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      $stmt->bind_param('i', $memberid);       
      /* Execute statement */
      $stmt->execute();  
}


function updateFamily ($memberid, $fname, $frelation, $fmonth, $fyear) {

  include 'includes/c1.php'; 

      // Now we insert form data PREPARE sql STATEMENT. 4 variables
      $insertsql = "INSERT INTO family (memberid, fname, frelation, fmonth, fyear) VALUES ( ?, ?, ?, ?, ?)";
      /* Prepare statement */
      $stmt = $c1->prepare($insertsql);
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $insertsql . ' Error: ' . $c1->error, E_USER_ERROR);
      }
       
      /* Bind the 42 parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      $stmt->bind_param('isssi', $memberid, $fname, $frelation, $fmonth, $fyear);
       
      /* Execute statement */
      $stmt->execute();  
}

function insertfamily ($memberid, $fname, $frelation, $fmonth, $fyear) {

  include 'includes/c1.php'; 
          // PREPARE sql STATEMENT. 4 variables
      $insertsql = "INSERT INTO family (memberid, fname, frelation, fmonth, fyear) VALUES ( ?, ?, ?, ?, ?)";

      /* Prepare statement */
      $stmt = $c1->prepare($insertsql);
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $insertsql . ' Error: ' . $c1->error, E_USER_ERROR);
      }
       
      /* Bind the 42 parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      $stmt->bind_param('isssi', $memberid, $fname, $frelation, $fmonth, $fyear);
       
      /* Execute statement */
      $stmt->execute();  
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

function pp($a) {
      echo "<pre>";
      print_r($a);
      echo "</pre>";
      exit();
}

function getMember($id) {
  global $c1;
  
  $sql = "select lastname, firstname from members where memberid=$id limit 1";
  $result = $c1->query($sql);
  $row = $result->fetch_assoc();
  $name = $row['firstname'].' '.$row['lastname'];
  return $name;
  }



function getMembers($id) {
  global $c1;
//query to get user id's
$sql = "SELECT m.memberid, concat( m.lastname,', ',m.firstname) as name, concat(m.address,',',m.city) as address  ,m.phone, m.email
FROM members m
INNER JOIN volunteer v
ON m.memberid=v.memberid
where v.eventsid=$id 
order by m.lastname asc";

$results = $c1->query($sql);
 //$row2 = $results->fetch_assoc(); 
 
return $results;

}//end getMembers()

function getGroupvolunteers($eventid) {
  global $c1;
  
  $sql = "select * from members where volunteer=$eventid order by lastname asc";
  $results = $c1->query($sql);
  return $results;
  }

function getGroupNames() {
  global $c1;
  
  $sql = " select eventname, eventsid from events order by eventname asc";
  $results = $c1->query($sql);
  return $results;
  }

function getMemberNames() {
  global $c1;
  
  $sql = "select memberid, concat(lastname,',  ',firstname,',   ',address,',  ',city) as info from members order by lastname asc";
  $results = $c1->query($sql);
  return $results;
  }

function getNonAdminMemberNames() {
  global $c1;
  
  $sql = "select memberid, concat(lastname,',  ',firstname,',   ',address,',  ',city) as info from members where admin=0 order by lastname asc";
  $results = $c1->query($sql);
  return $results;
  }

function getFamilyMembers($id) {
  global $c1;
  
  $sql = "select fname, frelation, fmonth, fyear from family where memberid=$id";
  $results = $c1->query($sql);
  return $results;
  }

  function getNewMemberPw() {
  global $c1;
  
  $sql = "select newmemberpw from admin";
  $r = $c1->query($sql);
  $row = @mysqli_fetch_assoc($r);
  $pw = $row['newmemberpw'];

  return $pw;
  }

function getCity($n) {
 $r = '';
  
 switch ($n) {
    case 1 :
      $r = 'DC';
      break;

     case 2 :
      $r = 'MD';
      break;

      case  3:
      $r = 'VA';
      break;

case 4 :
      $r = 'AL';
      break;

case 5 :
      $r = 'AS';
      break;

case 6 :
      $r = 'AZ';
      break;

case 7 :
      $r = 'AK';
      break;

case 8 :
      $r = 'CA';
      break;

case 9 :
      $r = 'CO';
      break;

case 10 :
      $r = 'CT';
      break;

case 11 :
      $r = 'DE';
      break;

case 12 :
      $r = 'FL';
      break;

case 13 :
      $r = 'GA';
      break;

case 14 :
      $r = 'HI';
      break;

case 15 :
      $r = 'ID';
      break;

case 16 :
      $r = 'IL';
      break;

case 17 :
      $r = 'IN';
      break;

case 18 :
      $r = 'IA';
      break;

case 19 :
      $r = 'KS';
      break;

case 20 :
      $r = 'KY';
      break;

case 21 :
      $r = 'LS';
      break;

case  22 :
      $r = 'ME';
      break;

case 23 :
      $r = 'MA';
      break;

case 24 :
      $r = 'MI';
      break;

case 25 :
      $r = 'MN';
      break;

case 26 :
      $r = 'MS';
      break;

case 27 :
      $r = 'MO';
      break;

case 28 :
      $r = 'MT';
      break;

case 29 :
      $r = 'NE';
      break;

case 30 :
      $r = 'NV';
      break;

case 31 :
      $r = 'NH';
      break;

case  32 :
      $r = 'NJ';
      break;

case 33 :
      $r = 'NM';
      break;

case 34 :
      $r = 'NY';
      break;

case 35 :
      $r = 'NC';
      break;

case 36 :
      $r = 'ND';
      break;

case 37 :
      $r = 'OH';
      break;

case 38 :
      $r = 'OK';
      break;

case 39 :
      $r = 'OR';
      break;

case 40 :
      $r = 'PA';
      break;

case 41 :
      $r = 'RI';
      break;

case 42 :
      $r = 'SC';
      break;

case 43 :
      $r = 'SD';
      break;

case 44 :
      $r = 'TN';
      break;

case 45 :
      $r = 'TX';
      break;

case 46 :
      $r = 'UT';
      break;

case 47 :
      $r = 'VT';
      break;

case 48 :
      $r = 'WA';
      break;

case 49 :
      $r = 'WV';
      break;

case 50 :
      $r = 'WI';
      break;

default:
  $r = "unknown";
  } 
    

  return $r;
  }

function getPropulsion($n) {
 $r = '';
  
 switch ($n) {
    case 1 :
      $r = 'Motor';
      break;

     case 2 :
      $r = 'Sail';
      break;

      case  3:
      $r = 'Electric';
      break;

default:
  $r = "Not Set";
  }     

  return $r;
  }

function getFueltype($n) {
 $r = '';
  
 switch ($n) {
    case 1 :
      $r = 'Gas';
      break;

     case 2 :
      $r = 'Diesel';
      break;

default:
  $r = "Not Set";
  }     

  return $r;
  }

  function getElectrical($n) {
 $r = '';
  
 switch ($n) {
    case 0 :
      $r = 'None';
      break;

     case 1 :
      $r = '20 Amp';
      break;

      case  2:
      $r = '30 Amp';
      break;

      case  3:
      $r = '50 Amp';
      break;


default:
  $r = "Not Set";
  }     

  return $r;
  }

 function getMonth($n) {
 $r = '';
  
 switch ($n) {    

     case 1 :
      $r = 'January';
      break;

      case  2:
      $r = 'February';
      break;

      case  3:
      $r = 'March';
      break;

      case 4 :
      $r = 'April';
      break;


      case 5 :
      $r = 'May';
      break;


      case 6 :
      $r = 'June';
      break;


      case 7 :
      $r = 'July';
      break;


      case 8 :
      $r = 'August';
      break;


      case 9 :
      $r = 'September';
      break;


      case 10 :
      $r = 'October';
      break;


      case 11 :
      $r = 'November';
      break;


      case  12 :
      $r = 'December';
      break;



default:
  $r = "Not Set";
  }     

  return $r;
  }

function getTypeFile($n) {
 $r = '';
  
 switch ($n) {    

     case 0 :
      $r = 'Board Minutes';
      break;

      case  1:
      $r = 'Membership Minutes';
      break;

      case  2:
      $r = 'Miscellaneous';
      break;

default:
  $r = "Not Set";
  }     

  return $r;
  }


//***************************************************************************************************************************
function getCountry() {
  global $c1;

$results = $c1->query("SELECT * FROM members");

if (mysqli_num_rows($results) > 0 )
  {
    
     return $results;
  }   

    return false;
} //end getMarkers//***************************************************************************************************************************

//***************************************************************************************************************************
function countryOptions() {
  global $c1;

$results = $c1->query("SELECT * FROM country");

if (mysqli_num_rows($results) > 0 )
  {
    
     return $results;
  }   

    return false;

}

 ?>
